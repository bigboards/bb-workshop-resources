# BigBoards Workshop Resources
This repository contains all the resources needed during the workshop. There are
two immediate subfolders available; solutions and exercises.

The goal of the workshop is to build a simple data pipeline using several common
big data technologies. It will work with a weather and a retail dataset hosted 
on an external server for now.

## Technologies
The following technologies are being handled as part of the workshop:

 * Hadoop (HDFS + MAPREDUCE)
 * Sqoop
 * Pig
 * Spark
 * Spark SQL
 * Spark Mllib
 
## Presentation
There are two presentations given as part of this workshop.
 * [BigData Basics - Commons](https://drive.google.com/open?id=1RqklSUIA1X6kW5UCPzgBAagf8YEV5eQhKB665Sx98_8) 
gives a quick overview of what bigdata is and what it is being used for.
 * [BigData Basics - Building a Data Pipeline](https://drive.google.com/open?id=1LZadkLa4bnPerLE9ckaKR4ibrQxw5PiApnHeB2I27fQ) 
will guide you into building your own data pipeline. It will discuss the technologies 
used for doing so. In combination with the resources in this project and a hex 
it provides you with everything you need to get your hands dirty.

## Use by others
You are free to use the content, presentation and resources from the workshop, but
keep in mind that we have put an aweful lot of work in creating these artifacts. Please
mention us if you can and spread the karma.

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons-Licentie" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">BigData Basics</span> van <a xmlns:cc="http://creativecommons.org/ns#" href="http://bigboards.io" property="cc:attributionName" rel="cc:attributionURL">BigBoards</a> is in licentie gegeven volgens een <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Naamsvermelding 4.0 Internationaal-licentie</a>.<br />Gebaseerd op een werk op <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/bigboards/bb-workshop-resources" rel="dct:source">https://gitlab.com/bigboards/bb-workshop-resources</a>.